# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the emby.bhdouglass package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: emby.bhdouglass\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-14 05:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:56
msgid "Swipe up from the bottom to set your Emby server's url"
msgstr ""

#: ../qml/Main.qml:63 ../qml/Main.qml:106
msgid "Settings"
msgstr ""

#: ../qml/Main.qml:149
msgid "Emby server url (ex: http://192.168.1.2:8096)"
msgstr ""

#: emby.desktop.in.h:1
msgid "Emby"
msgstr ""
